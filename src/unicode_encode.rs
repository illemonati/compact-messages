use std::io::{self, BufRead};
use std::{
    ops::{Generator, GeneratorState},
    pin::Pin,
};

// codepoints 0-31 are not usuable in messages, so there can only be (65535-32)/256 = 255.875 bytes per
// This naive version disregards that 0.875 and stores only 255 bytes per codepoint
//
// ACTUALLY this is stupid and doesnt work since 1 + 3 = 4 but from 4 you cannot derive the original was 1 and 3
pub fn unicode_encode_naive<'a>(
    message_reader: &'a mut impl BufRead,
    message_length: usize,
) -> impl Iterator<Item = char> + 'a {
    std::iter::from_generator(move || {
        let mut length = message_length / 255;
        let bytes_in_last_codepoint = (message_length % 255) as u32;
        if bytes_in_last_codepoint > 0 {
            length += 1;
        }

        //1 char for control character
        length += 1;

        yield char::from_u32(bytes_in_last_codepoint + 31).unwrap();

        for _group_num in (0..message_length).skip(255) {
            let mut codepoint: u32 = 31;
            let mut group_bytes = [0u8; 255];
            let read_size = message_reader.read(&mut group_bytes).unwrap();
            for group_byte in 0..read_size {
                let byte = group_bytes[group_byte];
                codepoint += byte as u32;
            }
            let codepoint_char = char::from_u32(codepoint).unwrap();
            yield codepoint_char;
        }
    })
}
