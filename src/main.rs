#![feature(generators, generator_trait)]
use std::{
    fs::File,
    io::{BufRead, BufReader, BufWriter, Write},
};

use compact_messages::unicode_encode;

fn main() {
    let file = File::open("./samples/sample-image.png").expect("unable to open file");
    let metadata = file.metadata().expect("unable to get metadata");
    let mut reader = BufReader::new(file);
    // let result = unicode_encode::unicode_encode_naive(&mut reader, metadata.len() as usize)
    //     .expect("read error");

    let generator = unicode_encode::unicode_encode_naive(&mut reader, metadata.len() as usize);

    let outfile = File::create("./samples/sample-image.png.txt").expect("err open outfile");
    let mut writer = BufWriter::new(outfile);

    let outstr: String = generator.collect();
    writer.write_all(outstr.as_bytes()).expect("write string");

    // for char in generator {
    //     let mut char_bytes: Vec<u8> = Vec::new();
    //     char.encode_utf8(&mut char_bytes);
    //     writer.write(&char_bytes).expect("error write");
    // }
}
